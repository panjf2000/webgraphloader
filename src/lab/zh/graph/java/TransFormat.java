package lab.zh.graph.java;
import lab.pan.graph.java.*;

import java.io.File;
import java.io.IOException;
import java.lang.*;

import org.apache.commons.math3.ml.clustering.DoublePoint;

import Jama.Matrix;
import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.webgraph.*;

public class TransFormat {
	//Print nodes
	final boolean print=false;
	//a progress logger used while loading the graph
	final ProgressLogger pl = new ProgressLogger();
	
	public static void main(String[] args) {
		new  TransFormat().loadSequencially("./file/amazon-2008-hc");
	}
	/*
	 * @Info load graph without random access ability
	 */
	public void loadSequencially(String path){
		try {
			//The time interval for a new log in milliseconds
			pl.logInterval = 100L;
			
			BVGraph graph = BVGraph.load(path,0,pl);
			final int n = graph.numNodes();
			
			pl.start( "Starting visit..." );
			//The number of expected calls to {@link #update()}
			pl.expectedUpdates = n-0;
			pl.itemsName = "nodes";
			
			NodeIterator nodeIterator = graph.nodeIterator();
			StringBuilder content = new StringBuilder(); 
			while(nodeIterator.hasNext()){
				int node =nodeIterator.nextInt();
				//content.append(String.valueOf(node)+"  ");
				if(print)
					System.out.println("node"+node);
				LazyIntIterator lazyIterator = nodeIterator.successors();
				int outDegreeNode = 0;
				/*if((outDegreeNode=lazyIterator.nextInt())!=-1)
					content.append(String.valueOf(outDegreeNode)+"\n");*/
				while((outDegreeNode = lazyIterator.nextInt())!=-1){
					content.append(String.valueOf(node)+"\t"+String.valueOf(outDegreeNode)+"\n");
					if(print)
						System.out.println("----->"+outDegreeNode);
				}
				pl.update();
			}
			
			
		  try{
			FileUtils.mkdir("data");    
			FileUtils.mkdir("zipData");
			FileUtils.createNewFile("./data/graphNode.txt", content.toString());
			File file=new File("./data/graphNode.txt");
			String size;
			if((size=FileUtils.getFileSize(file)).endsWith("MB"))
				if(Double.parseDouble(size.substring(0, size.indexOf(" MB")))>=50)
					FileUtils.zip("./data/","./zipData/");
		  }catch(IOException e)
		  {
		      e.printStackTrace();
		  }
			pl.done();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    public void opMatrix(String Path)
    {
    	try{
    	pl.logInterval=100L;
    	BVGraph graph = BVGraph.load(Path,0,pl);
    	final int n = graph.numNodes();
    	double[][] adjacencyMatrix=new double[n][n];
    	for(double[] r:adjacencyMatrix)
    		for(int i=0;i<r.length;i++)
    			r[i]=0;
		pl.start( "Converting..." );
		
		pl.expectedUpdates = n-0;
		pl.itemsName = "Matrix";
		NodeIterator ni=graph.nodeIterator();
		while(ni.hasNext())
		{
			int node=ni.nextInt();
		    int[] nodeArray=ni.successorArray();
		    for(int i:nodeArray)
		    {
		    	adjacencyMatrix[node][i]=1;
		    }
		}
		pl.update();
		pl.done();
		Matrix M=new Matrix(adjacencyMatrix,n,n);
    	}catch(IOException e)
    	{
    		e.printStackTrace();
    	}
    }
}
