package lab.pan.graph.java;

import java.awt.HeadlessException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JApplet;
import javax.swing.JFrame;

import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.webgraph.*;

public class DisplayGraph extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2964714216431010367L;

	public DisplayGraph() throws HeadlessException {
		// TODO Auto-generated constructor stub
		super();
		
		initGraph("C:\\Users\\Pan\\webgraphloader\\file\\amazon-2008-hc");
		add(new GraphView(graph));
	}
	final static ProgressLogger pl = new ProgressLogger();
	private static Node[] Nodes;
	private static int[][] edges;
	private static Graph<Node> graph;
	
	private static void initGraph(String graphPath) {
		try {
			pl.logInterval = 100L;
			BVGraph bvGraph = BVGraph.load(graphPath,0,pl);
			final int n = bvGraph.numNodes();
			System.out.println(n);
			pl.start( "Starting visit..." );
			//The number of expected calls to {@link #update()}
			pl.expectedUpdates = n-0;
			pl.itemsName = "nodes";
			Nodes = new Node[n];
			for(int i=0;i<n;i++)
				Nodes[i]=new Node(String.valueOf(i), (int)Math.random()*1000000, (int)Math.random()*1000000);
			Set<String> line = FileUtils.readFileLine("C:\\Users\\Pan\\webgraphloader\\data\\graphNode.txt");
			Iterator<String> iterator = line.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				String str = iterator.next();
				String[] numString = str.split("\t");
				edges[i++][0] = Integer.parseInt(numString[0]);
				edges[i++][1] = Integer.parseInt(numString[1]);
			}
			graph = new UnweightedGraph<Node>(edges, Nodes);
			pl.done();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	static class Node implements Displayable {
		private int x, y;
		private String name;

		Node(String name, int x, int y) {
			this.name = name;
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public String getName() {
			return name;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 JFrame frame = new JFrame("DisplayGraph");
		    DisplayGraph applet = new DisplayGraph();
		    frame.add(applet);
		    applet.init();
		    applet.start();

		    frame.setLocationRelativeTo(null);
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setSize(800, 500);
		    frame.setVisible(true);

	}
}
