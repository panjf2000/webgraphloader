/**
 * 
 */
package lab.pan.graph.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.unimi.dsi.webgraph.*;
import it.unimi.dsi.logging.ProgressLogger;

/**
 * @author Pan
 *
 */
public class Test {
	final static ProgressLogger pl = new ProgressLogger();
	private static int[][] edges;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//loadGraph("./file/amazon-2008-hc");
		try
		{
		Set<String> line = FileUtils.readFileLine("C:\\Users\\Pan\\webgraphloader\\data\\graphNode.txt");
		int i = 0;
		Iterator<String> iterator = line.iterator();
		while (iterator.hasNext()) {
			String str = iterator.next();
			Pattern p = Pattern.compile("\t"); 
			Matcher m = p.matcher(str); 
			//保存结果数组  
	        List<String> ret = new ArrayList<String>();  
	        //临时变量  
	        String temp = null;  
	        int index = 0;  
	        while(m.find())  
	        {  
	            int start = m.start();  
	                          
	            temp = str.substring(index, start);  
	            ret.add(temp);  
	                          
	            index = m.end();  
	              
	        }  
			//String[] numString = str.split('\t');
			edges[i++][0] = Integer.parseInt(ret.get(0));
			edges[i++][1] = Integer.parseInt(ret.get(1));
			System.out.println(ret.get(0)+"  "+ret.get(1));
		}
		
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		
		
	

	}

	public static void loadGraph(String graphPath)
	{
		pl.logInterval=100L;
		try{
		BVGraph graph = BVGraph.load(graphPath,0,pl);
    	final int n = graph.numNodes();
    	pl.start( "Loading..." );
    	pl.expectedUpdates = n-0;
		pl.itemsName = "loadGraph";
		//NodeIterator ni=graph.nodeIterator();
		/*while(ni.hasNext())
		{
			int node =ni.nextInt();
		    int [] nodeArray=ni.successorArray();
		    System.out.print("node"+node+"：");
		    for(int i:nodeArray)
		    	System.out.print(+i+"|");
		}*/
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
